package utils;

import avro.shaded.com.google.common.collect.Lists;
import entity.Country;
import entity.WeekInfo;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.Row;
import scala.Tuple2;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.*;
import java.util.stream.Collectors;

public class Utils {

    static InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream("app.properties");
    public static Properties prop = new Properties();

    /**
     * initialization of properties file
     */
    public static void initProperties() {

        try {
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param row row line
     * @return tuple: (weekID, weekCovidMainData)
     */
    public static Tuple2<String, WeekInfo> rowToTuple(Row row) {

        String weekKey = row.getString(0);

        return new Tuple2<>(weekKey, new WeekInfo(Integer.parseInt(row.get(1).toString()), Integer.parseInt(row.get(2).toString())));
    }

    /**
     * @param csvLine processed csv line of dataset3
     * @return tuple object with filled data <weekKey, weekInfo>
     */
    public static Tuple2<String, WeekInfo> parseCSV1(String csvLine) {

        String[] csvValues = csvLine.split(",");
        return new Tuple2<>(csvValues[0], new WeekInfo(Integer.parseInt(csvValues[1]),Integer.parseInt(csvValues[2])));
    }

    /**
     * @param line processed csv line of dataset2
     * @return country object with continent, location, regression slope, aggregate cases per week
     */
    public static Country parseCSV2(String line) {

        final int COL_START_DAYNEWCASES_IDX = 2;        //starting column values index in csv
        String[] csvValues = line.split(",");
        String continent = csvValues[0];
        String locationName = csvValues[1];
        List<Integer[]> aggregatedCasesPerWeek = new ArrayList<>();
        Integer[] weekCasesConfirmed = new Integer[7];

        //using all dataset => first increment = first day value
        int iesterdayCumulative = 0;
        int todayConfirmedCumulative, todayNewConfirmed;
        int dayID = 0;

        SimpleRegression regression = new SimpleRegression();

        //for all days cumulate confirmed cases per week and compute linear regression of new cases per day
        for (int i = COL_START_DAYNEWCASES_IDX; i < csvValues.length; i++, iesterdayCumulative = todayConfirmedCumulative, dayID++) {

            //weekIDOfDay = dayID/7;
            if (dayID % 7 == 0 && dayID > 0) { //NB day start from 0

                aggregatedCasesPerWeek.add(weekCasesConfirmed);             //append ended week to the aggregation list
                weekCasesConfirmed = new Integer[7];                        //prepare for the new  week
            }

            todayConfirmedCumulative = Integer.parseInt(csvValues[i]);
            todayNewConfirmed = todayConfirmedCumulative - iesterdayCumulative;
            regression.addData(dayID, todayNewConfirmed);                   //cumulate observation for the linear regression of the increments
            weekCasesConfirmed[dayID % 7] = todayConfirmedCumulative;
        }

        double regressionSlope = regression.getSlope();

        return new Country(continent, locationName, regressionSlope, aggregatedCasesPerWeek);
    }

    /**
     * @param aggretedCases1   list x
     * @param aggregatedCases2 list y
     * @return list of the summed ith values
     */
    public static List<Integer[]> listReducerAggregateCases(List<Integer[]> aggretedCases1, List<Integer[]> aggregatedCases2) {

        List<Integer[]> out = new ArrayList<>();

        for (int i = 0; i < aggretedCases1.size(); i++) {

            Integer[] week = new Integer[7];

            for (int j = 0; j < 7; j++) {

                week[j] = aggretedCases1.get(i)[j] + aggregatedCases2.get(i)[j];
            }

            out.add(week);
        }

        return out;
    }

    /**
     * @param aggregatedCasesPerWeek aggregate cases per week
     * @return iterator of cases distinct by (continent, week)
     */
    public static Iterator<Country> expandAggregateCases(Tuple2<String, List<Integer[]>> aggregatedCasesPerWeek) {

        List<Country> countriesWithWeekCases = new ArrayList<>();

        for (int i = 0; i < aggregatedCasesPerWeek._2.size(); i++) {

            countriesWithWeekCases.add(new Country(aggregatedCasesPerWeek._1, aggregatedCasesPerWeek._2.get(i), i));
        }

        return countriesWithWeekCases.iterator();
    }

    /**
     * @param country object with continent, week, week info
     * @return object stats (min, max, average, std deviation)
     */
    public static String stats(Country country) throws ParseException {

        double dev, mean;
        int min, max;
        double[] weekCases = new double[7];

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("w-yyyy-MM-dd");
        java.util.Date firstDayQuery2 = simpleDateFormat.parse(prop.getProperty("FIRST_DAY_QUERY2"));
        Calendar cal = Calendar.getInstance();
        cal.setTime(firstDayQuery2);
        cal.add(Calendar.WEEK_OF_YEAR, country.getWeekIdOfCases());

        String weekKey = simpleDateFormat.format(cal.getTime());

        for (int i = 0; i < 7; i++)
            weekCases[i] = country.getCasesOfWeek()[i];

        min = (int) StatUtils.min(weekCases);
        max = (int) StatUtils.max(weekCases);
        mean = StatUtils.mean(weekCases);
        dev = Math.sqrt(StatUtils.populationVariance(weekCases));

        return country.getContinent() + "," + weekKey + "," + min + "," + max + "," + mean + "," + dev;
    }

    /**
     * @param line processed csv line of dataset 2 for query with Continent,Country,Confirmed31Gen,Confirmed1Feb....
     * @return country obj with aggregated cases per month
     */
    public static Iterator<Country> parseCSV3(String line, int firstYear) {

        final int firstMonth = 2;
        int monthLen = YearMonth.of(firstYear, firstMonth).lengthOfMonth();
        final int COL_START_DAYNEWCASES_IDX = 4;        //starting column values index in csv
        String[] csvValues = line.split(",");
        String locationName = csvValues[1];
        List<Country> countryCasesAggregatedPerMonth = new ArrayList<>();
        Integer[] monthCases = new Integer[monthLen];

        // take first cumulative val to compute newCases per day
        int iesterdayCumulative = Integer.parseInt(csvValues[COL_START_DAYNEWCASES_IDX - 1]);
        int todayConfirmedCumulative, todayNewConfirmed;
        int dayID = 0, month = firstMonth, year = firstYear;
        int progressiveMonthId = 0;

        //for all other days cumulate confirmed cases per month
        for (int i = COL_START_DAYNEWCASES_IDX; i < csvValues.length; i++, iesterdayCumulative = todayConfirmedCumulative, dayID++) {

            if (dayID == monthLen) {

                countryCasesAggregatedPerMonth.add(new Country(monthCases, locationName, progressiveMonthId++)); //append ended month to the aggregation list
                month++;

                //handle next year
                if (month == 13) {

                    month = 1;
                    year++;
                }

                monthLen = YearMonth.of(year, month).lengthOfMonth();
                monthCases = new Integer[monthLen];                                                 //prepare for the new  month
                dayID = 0;
            }

            todayConfirmedCumulative = Integer.parseInt(csvValues[i]);
            todayNewConfirmed = todayConfirmedCumulative - iesterdayCumulative;
            monthCases[dayID] = todayNewConfirmed;
        }

        final int MIN_DAYS_PARTIAL_MONTH = 15;      //min days to consider an incomplete month

        //parsing of incomplete months if longer than a treshold
        Integer[] lastMonth = new Integer[dayID];
        if (dayID >= 0) System.arraycopy(monthCases, 0, lastMonth, 0, dayID);

        if (dayID != monthLen && dayID > MIN_DAYS_PARTIAL_MONTH)
            countryCasesAggregatedPerMonth.add(new Country(lastMonth, locationName, progressiveMonthId));        //append ended month to the aggregation list

        return countryCasesAggregatedPerMonth.iterator();
    }

    /**
     * @param monthGroup      month with iterable tuples to truncate
     * @param limitEntriesNum truncation value n
     * @return top n tuples per month
     */
    public static Tuple2<Integer, List<Tuple2<Double, String>>> topMonthlyEntries(Tuple2<Integer, Iterable<Tuple2<Double, String>>> monthGroup, int limitEntriesNum) {

        List<Tuple2<Double, String>> monthCountries = Lists.newArrayList(monthGroup._2);
        monthCountries.sort(new MonthTrendlineComparator());
        List<Tuple2<Double, String>> monthTop50Countries;
        monthTop50Countries = monthCountries.stream().limit(limitEntriesNum).collect(Collectors.toList());

        return new Tuple2<>(monthGroup._1, monthTop50Countries);
    }

    /**
     *expand the list inside the tuple in separated elements
     */
    public static Iterator<Vector> expandTupleList(Tuple2<Integer, List<Tuple2<Double, String>>> tuple) {

        List<Vector> list = new ArrayList<>();

        for (int i = 0; i < tuple._2.size(); i++) {

            double[] temp = new double[]{tuple._2.get(i)._1};
            list.add(Vectors.dense(temp));
        }

        return list.iterator();
    }

    /**
     * @param processGroupID process group id of target process group to start by nifi rest API
     * @return true if the activation has succeeded, false otherwise
     */
    public static Boolean nifiProcessRestStart(String processGroupID) throws Exception {

        URL url = new URL(prop.getProperty("NIFI_ADDR_BASE") + processGroupID);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setRequestMethod("PUT");
        httpCon.setDoOutput(true);
        //nifi start process group: content json header, json{id:groupID,state:TARGET_STATE}
        httpCon.setRequestProperty("Content-Type", "application/json");
        String putData = "{\"id\":\"" + processGroupID + "\",\"state\":\"RUNNING\"}";
        OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
        out.write(putData);
        out.close();
        httpCon.disconnect();

        return httpCon.getResponseCode() < 300;
    }
}




