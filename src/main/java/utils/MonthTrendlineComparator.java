package utils;

import scala.Serializable;
import scala.Tuple2;

public class MonthTrendlineComparator implements java.util.Comparator<scala.Tuple2<Double,String>>, Serializable {

    @Override
    public int compare(Tuple2<Double, String> o1, Tuple2<Double, String> o2) {

        if (o1._1 < o2._1) {

            return 1;

        } else if (o1._1.equals(o2._1))

            return 0;

        return -1;
    }
}
