package utils;

import entity.Country;
import java.io.Serializable;
import java.util.Comparator;

public class CountryComparator implements Comparator<Country>, Serializable {

    @Override
    public int compare(Country o1, Country o2) {

        double trendline1 = o1.getTrendline(), trendlin2 = o2.getTrendline();

        if (trendline1 < trendlin2) {

            return 1;

        } else if (trendline1 == trendlin2)

            return 0;

        return -1;
    }
}
