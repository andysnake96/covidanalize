import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;
import entity.Country;
import utils.CountryComparator;
import java.net.URI;
import java.util.*;

import static utils.Utils.*;

public class Query2 {

    public static void main(String[] args) {

        initProperties();

        String fileSystem = prop.getProperty("HDFS");
        String pathToDataset2 = prop.getProperty("PATH_TO_DATASET2_QUERY2");
        String outputPathResultsQuery2 = prop.getProperty("OUTPUT_PATH_QUERY2_RESULTS");
        int topCountriesQuery2Num = Integer.parseInt(prop.getProperty("TOP_COUNTRIES_QUERY2"));
        String sparkMaster = prop.getProperty("SPARK_MASTER");
        SparkConf conf = new SparkConf().setMaster(sparkMaster).setAppName("Query2");
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.setLogLevel("OFF");

        JavaRDD<String> dataset2Query2 = sc.textFile(pathToDataset2);

        //increments linear regression calculation and info filling for each country
        JavaRDD<Country> sortedByTrendline = dataset2Query2.map(line -> parseCSV2(line));

        //take top n countries sorted by trendline
        List<Country> topNCountries = sortedByTrendline.takeOrdered(topCountriesQuery2Num, new CountryComparator());
        JavaRDD<Country> topNCountriesRDD = sc.parallelize(topNCountries);

        //aggregate top countries cases per week
        //RDD tuple: <continent, aggregateCasesPerWeek>
        JavaPairRDD<String, List<Integer[]>> continentsAggregateCases = topNCountriesRDD
                .mapToPair(country -> new Tuple2<>(country.getContinent(), country.getAggregateCases()))
                .reduceByKey((x, y) -> listReducerAggregateCases(x, y));

        //expand continent tuples to <continent, weekID, 7DayWeekCases>
        JavaRDD<Country> continentWeekCases = continentsAggregateCases.flatMap((FlatMapFunction<Tuple2<String, List<Integer[]>>, Country>)
                weekAggregateCases -> expandAggregateCases(weekAggregateCases));

        //compute query statistics in csv format
        JavaRDD<String> output = continentWeekCases.map(line -> stats(line));

        for (String line: output.collect())
            System.out.println(line);

        //delete old hdfs output dir
        if (Boolean.parseBoolean(prop.getProperty("DELETE_HDFS_OLD_DIR"))) {

            try {

                FileSystem hdfs = FileSystem.get(new URI(fileSystem), new Configuration());
                hdfs.delete(new org.apache.hadoop.fs.Path(outputPathResultsQuery2), true);

            } catch (Exception e) {

                System.err.println("Unable to delete old folder at" + outputPathResultsQuery2);
                e.printStackTrace();
            }
        }

        //storing results to hdfs
        output.saveAsTextFile(outputPathResultsQuery2);

        //exporting output data from hdfs to Hbase
        if (Boolean.parseBoolean(prop.getProperty("PUT_RESULTS_HBASE"))) {

            String queryProcessGroup = prop.getProperty("PROCESS_GROUP_WRITE_QUERY2");

            try {

                System.out.println("NIFI STORE RESULT TO HBASE SUCCESS: " + nifiProcessRestStart(queryProcessGroup));

            } catch (Exception e) {

                System.err.println("Unable to start nifi process group: " + queryProcessGroup);
                e.printStackTrace();
            }
        }

        sc.stop();
    }
}