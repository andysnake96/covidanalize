package kmeans;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import scala.Serializable;
import scala.Tuple2;

import java.util.*;

import static utils.Utils.*;

/**
 * implementation of Naive KMeans and wrap of MLlib kmeans RDDbased API
 * to cluster top countries by trendline value of covid19 cases increments for each month
 */
public class Kmeans implements Serializable {

    private int clustersNumber;
    private int maxIterations;
    private int clusterPointsNum;

    public Kmeans(int clustersNumber, int maxIterations, int clusterPointsNum) {
        this.clustersNumber = clustersNumber;
        this.maxIterations = maxIterations;
        this.clusterPointsNum = clusterPointsNum;
    }

    /**
     * @param centroids centroids values to gather
     * @param centroidsNum expected number of centroids point
     * @return gathered centroids extended with 0 if less than centroids num
     */
    private Double[] gatherCentroidsArray(List<Double> centroids, int centroidsNum) {

        //gather array of  centroidsNum centroids points. if centroids.size is fewer then centroidsNum -> fill the rest with 0 (empty clusters founded)
        Double[] out = new Double[centroidsNum];

        for (int i = 0; i < centroids.size(); i++)
            out[i] = centroids.get(i);

        for (int i = centroids.size(); i < centroidsNum; i++)
            out[i] = (double) 0;

        return out;
    }

    /**
     * extract correspondence trendline value -> country name string from each trendline - country tuple in a given month
     * @param monthTrendlineCountry     monthID,List  of trendline values, countrynames
     * @return correspondeces trendline value -> country name
     */
    private HashMap<Double, String[]> monthTrendilneCountryMapper(Tuple2<Integer, List<Tuple2<Double, String>>> monthTrendlineCountry) {

        HashMap<Double, String[]> map = new HashMap<>();
        String[] newMapping;

        for (int i = 0; i < clusterPointsNum; i++) {

            String[] oldMapping = map.get(monthTrendlineCountry._2.get(i)._1);

            if (oldMapping == null) {

                newMapping = new String[]{monthTrendlineCountry._2.get(i)._2()};

            } else {

                newMapping = Arrays.copyOf(oldMapping, oldMapping.length + 1);
                newMapping[oldMapping.length] = monthTrendlineCountry._2.get(i)._2();
            }

            map.put(monthTrendlineCountry._2.get(i)._1, newMapping);
        }

        return map;
    }

    /**
     * for each month cluster cluster the top countries hitted by covid by their trendline points
     * with the MLlib RDD based API,sequential on each month
     * @param top50CountriesMonth   monthID,List <trendline,country>
     * @param firtsMonthId          start month num to format output
     * @param firstYear
     * @return  out csv: month,countryName,clustering centroid point
     */
    public String computeMLlibKMeans(List<JavaPairRDD<Integer, List<Tuple2<Double, String>>>> top50CountriesMonth, int firtsMonthId, int firstYear) {

        //countries trendlines points to cluster for each month
        List<JavaRDD<Vector>> monthTrendlineVectorPoints = new ArrayList<>();

        //maps: trendline -> country name for final gather for each month
        List<JavaRDD<HashMap<Double, String[]>>> trendlineByCountry = new ArrayList<>();

        //extract points to cluster for every month
        for (JavaPairRDD<Integer, List<Tuple2<Double, String>>> countryMonthTopCases : top50CountriesMonth) {   //for each month

            //expand every trendline value in the top50 countries to a point int the cluster
            monthTrendlineVectorPoints.add(countryMonthTopCases.flatMap(countriesCases -> expandTupleList(countriesCases)));

            //for each month build a correspondence hashmap for the final gather of the country name from a trendline value clustered
            trendlineByCountry.add(countryMonthTopCases.map(line -> monthTrendilneCountryMapper(line)));
        }

        List<KMeansModel> monthKMeansModelList = new ArrayList<>();             //centroids
        List<JavaRDD<Integer>> monthClusterMembersList = new ArrayList<>();     //clustering resoult indexd by cluster points, per month

        //Clustering per month
        for (JavaRDD<Vector> item : monthTrendlineVectorPoints) {

            monthKMeansModelList.add(KMeans.train(item.rdd(), clustersNumber, maxIterations));     //centroids
            monthClusterMembersList.add(KMeans.train(item.rdd(), clustersNumber, maxIterations).predict(item));
        }

        String outCsv = "";

        int month;
        int oldMonth = 0;
        int year = firstYear;

        //for each month collect: points, clustering results, correspondence hashmap
        for (int i = 0; i < monthKMeansModelList.size(); i++) {

            Vector[] monthCentroids = monthKMeansModelList.get(i).clusterCenters();

            List<Vector> monthPoints = monthTrendlineVectorPoints.get(i).collect();
            HashMap<Double, String[]> monthTrendlineByCountry = trendlineByCountry.get(i).collect().get(0);
            List<Integer> clusterMembershipIndexes = monthClusterMembersList.get(i).collect();

            month = (i + firtsMonthId - 1)%12;

            if (month < oldMonth)
                year++;

            oldMonth = month;

            //trendline -> occurences number in current month
            HashMap<Double, Integer> trendlinePointCollided = new HashMap<>();

            //for each country in the montly top 50 retrieve the target cluster
            for (int j = 0; j < clusterPointsNum; j++) {

                //get trendline point and corresponding state
                Vector point = monthPoints.get(j);
                Integer occurrence = trendlinePointCollided.get(point.apply(0));

                if (occurrence == null) {

                    occurrence = 0;
                }

                String[] targetStateCollided = monthTrendlineByCountry.get(point.apply(0));
                String targetState = targetStateCollided[occurrence];

                occurrence++;
                trendlinePointCollided.put(point.apply(0), occurrence); //update collision number

                //get point's cluster by indexing
                double clusterCentroidPoint = monthCentroids[clusterMembershipIndexes.get(j)].apply(0);

                outCsv+= 1 + month + "-" + year + "," + targetState + "," + clusterCentroidPoint+"\n";
            }
        }

        return outCsv;
    }

    /**
     * for each month cluster cluster the top countries hitted by covid by their trendline points
     * with the MLlib RDD based API,sequential on each month
     * @param top50CountriesMonth   monthID,List <trendline,country>
     * @param firtsMonthId          start month num to format output
     * @param firstYear
     * @return  out csv: month,countryName,clustering centroid point
     */
    public String computeMLlibKMeansConcurrent(List<JavaPairRDD<Integer, List<Tuple2<Double, String>>>> top50CountriesMonth, int firtsMonthId, int firstYear) throws InterruptedException {

        int numMonth = top50CountriesMonth.size();

        //countries trendlines points to cluster for each month
        JavaRDD<Vector>[] monthTrendlineVectorPoints = new JavaRDD[numMonth];

        //maps: trendline -> country name for final gather for each month
        JavaRDD<HashMap<Double, String[]>>[] trendlineByCountry = new JavaRDD[numMonth];

        //extract points to cluster for every month
        for (int m = 0; m < numMonth; m++) {

            JavaPairRDD<Integer, List<Tuple2<Double, String>>> countryMonthTopCases = top50CountriesMonth.get(m);

            //expand every trendline value in the top50 countries to a point int the cluster
            monthTrendlineVectorPoints[m] = countryMonthTopCases.flatMap(countriesCases -> expandTupleList(countriesCases));

            //for each month build a correspondence hashmap for the final gather of the country name from a trendline value clustered
            trendlineByCountry[m] = countryMonthTopCases.map(line -> monthTrendilneCountryMapper(line));
        }

        //Kmeans || in num of month, distribuited among nodes by MLlib
        Thread[] monthThreads = new Thread[numMonth];

        List<Vector>[] monthTrendlinesPoints = new List[numMonth];  // for each month, month trendline points
        HashMap<Double, String[]>[] monthTrendlineByCountry = new HashMap[numMonth];  //trendlineByCountry.get(i).collect().get(0);
        List<Integer>[] monthClusterMembershipIndexes = new List[numMonth];//monthClusterMembersList[i].collect();
        Vector[][] monthClusterCentroidsPoints = new Vector[numMonth][clustersNumber];

        //start 1 tread per month
        for (int m = 0; m < numMonth; m++) {

            int monthID = m;
            Thread t = new Thread(() -> {
                //train and predict KMeans model on monthTrendline points, gather resoults
                JavaRDD<Vector> trendLinesMonthPoints = monthTrendlineVectorPoints[monthID];
                KMeansModel model = KMeans.train(trendLinesMonthPoints.rdd(), clustersNumber, maxIterations);     //centroids
                JavaRDD<Integer> clustersIndexes = model.predict(trendLinesMonthPoints);
                Vector[] centroids = model.clusterCenters();
                // gather ||
                monthTrendlinesPoints[monthID] = trendLinesMonthPoints.collect();
                monthTrendlineByCountry[monthID] = trendlineByCountry[monthID].collect().get(0);
                monthClusterMembershipIndexes[monthID] = clustersIndexes.collect();
                monthClusterCentroidsPoints[monthID] = centroids;
            });

            t.start();
            monthThreads[m] = t;  //save thread object for later gather
        }

        //join threads
        for (int m = 0; m < numMonth; m++) monthThreads[m].join();

        String outCsv="";

        int month;
        int oldMonth = 0;
        int year = firstYear;

        for (int m = 0; m < numMonth; m++) {

            //for each country in the montly top 50 retrieve the target cluster
            month = (m + firtsMonthId - 1)%12;

            if (month < oldMonth)
                year++;

            oldMonth = month;

            //trendline -> occurences number in current month
            HashMap<Double, Integer> trendlinePointCollided = new HashMap<>();

            for (int j = 0; j < clusterPointsNum; j++) {

                //get trendline point and corresponding state
                Vector point = monthTrendlinesPoints[m].get(j);
                Integer occurrence = trendlinePointCollided.get(point.apply(0));

                if (occurrence == null) {

                    occurrence = 0;
                }

                String[] targetStateCollided = monthTrendlineByCountry[m].get(point.apply(0));
                String targetState = targetStateCollided[occurrence];

                occurrence++;
                trendlinePointCollided.put(point.apply(0), occurrence); //update collision number
                //get point's cluster by idexing
                Integer clusterId = monthClusterMembershipIndexes[m].get(j);
                double clusterCentroid = monthClusterCentroidsPoints[m][clusterId].apply(0);

                outCsv+= 1 + month + "-" + year + "," + targetState + "," + clusterCentroid+"\n";
            }
        }

        return outCsv;
    }

    /////////////////Naive implementation

    /**
     * get the nearest centroid to point
     * @param point  target point to search nearest centroid point
     * @param centroids centroid points
     * @return nearest centroid point
     */
    private Double nearestCentroids(Double point, Double[] centroids) {

        double nearest = centroids[0];
        double nearestDist = Math.abs(point - nearest);

        for (int i = 1; i < centroids.length; i++) {

            double d = Math.abs(point - centroids[i]);

            if (d < nearestDist) {

                nearest = centroids[i];
                nearestDist = d;
            }
        }

        return nearest;
    }

    /**
     * evaluate distance between 2 sets of centroids points
     * @param centroidsOld old centroids points
     * @param centroids     new centroids points
     * @return  max distance among centroids points (both sets sorted)
     * or infinite if one of the centroids is initialized (uninitialized centroids)
     */
    private Double centroidsDistances(Double[] centroidsOld, Double[] centroids) {

        if (centroids == null || centroidsOld == null)
            return Double.POSITIVE_INFINITY;  //inf if some centroids un initialized

        Arrays.sort(centroids);
        Arrays.sort(centroidsOld);

        double maxCentroidsDist = 0;
        double d;
        int comparableClusters = Math.min(centroidsOld.length, centroids.length);

        for (int x = 0; x < comparableClusters; x++) {

            d = Math.abs(centroidsOld[x] - centroids[x]);
            if (d > maxCentroidsDist) maxCentroidsDist = d;     //take max distance between sorted centroids points
        }

        return maxCentroidsDist;
    }

    ///starting centroids initializations
    /**
     * get centroidsNum random centroids extracted without replacement
     * */
    private Double[] getStartingCentroidsRand(JavaPairRDD<Double, String> points, int centroidsNum) {

        List<Tuple2<Double, String>> centroidsRand = points.takeSample(false, centroidsNum);
        Double[] centroids = new Double[centroidsNum];

        for (int i = 0; i < centroidsNum; i++)
            centroids[i] = centroidsRand.get(i)._1;

        return centroids;
    }

    /**
     * initialize centroids for all month trendline points given with this euristic
     * from sorted point get a random point in  a group of 3 points equally separated among all points
     * @param top50Month  tuple<monthID,List<trendline,countryName>
     * @return tuple<monthID,centroidPointsArray<trendlinePoint,countryName>
     */
    private Tuple2<Integer, Tuple2<Double, String>[]> centroidsInitSeparatedGroups(Tuple2<Integer, List<Tuple2<Double, String>>> top50Month) {

        Tuple2<Double, String>[] distantiatedPoints = new Tuple2[clustersNumber];
        int rndIdx = new Random().nextInt(3);

        for (int i = 0, k = 0; i < clusterPointsNum; i += 13, k++) {

            int targetIdx = Math.min(rndIdx + i, clusterPointsNum - 1);
            distantiatedPoints[k] = top50Month._2.get(targetIdx);         //init cluster centroid
        }

        return new Tuple2<>(top50Month._1, distantiatedPoints);
    }

    /**
     * CORE LOOP OF KMEANS NAIVE BASED ON LYOID ALGORITM
     * @param centroids  centroid points
     * @param tolerationCentroidsDistance distance threshold among old/new centroids to continue the Lyoid algo
     * @param points    points to cluster as <trendline,CountryName>
     * @param sc    java sparkContext to broadcast common variable  values in maps
     * @return clustered points list of <centroidPoint,<trendline,countryName>
     */
    private List<Tuple2<Double, Tuple2<Double, String>>> kmeansNaive(Double[] centroids, Double tolerationCentroidsDistance, JavaPairRDD<Double, String> points, JavaSparkContext sc) {

        Double[] centroidsOld = null;
        //classified points: clusterCentroid,<trendline,coutry>
        JavaPairRDD<Double, Tuple2<Double, String>> classifiedRDD = null;   //pre declared for later gather resoults
        int iterN = 0;

        while (iterN++ < maxIterations && centroidsDistances(centroidsOld, centroids) > tolerationCentroidsDistance) {
            centroidsOld = centroids;

            //classify: for each point find the nearest centroid -> associate the point to the centroid's cluster
            Broadcast<Double[]> centroidsBroadcasted = sc.broadcast(centroids);   //pass centroids array once per mapper

            //RDD of <centroidID,point<trendline,countryName>>
            classifiedRDD = points.mapToPair(point -> new Tuple2<>(nearestCentroids(point._1, centroidsBroadcasted.getValue()), point))
                    .cache();

            //update centroids, summing points values, for each cluster
            //RDD <centroidID,summedTrendlinePoints>
            JavaPairRDD<Double, Double> summerRDD = classifiedRDD.mapToPair(pointWithCountry -> new Tuple2<>(pointWithCountry._1, pointWithCountry._2._1)).
                    reduceByKey((point0, point1) -> point0 + point1);

            //get clusters sizes (for later averaging)

            //clusterCentroid -> numElements mapped to it
            Map<Double, Long> clusterSizes = classifiedRDD.countByKey();

            Broadcast<Map<Double, Long>> clusterSizesBroadcased = sc.broadcast(clusterSizes);

            //recentroids averaging points per cluster
            JavaRDD<Double> centroidsNew = summerRDD.map(pointsSummed -> pointsSummed._2 / clusterSizesBroadcased.getValue().get(pointsSummed._1));

            //update centroids
            centroids = gatherCentroidsArray(centroidsNew.collect(), clustersNumber);
        }

        List<Tuple2<Double, Tuple2<Double, String>>> clusteredPointsAndCentroids = classifiedRDD.collect();

        //unpersist not anymore used rdds
        classifiedRDD.unpersist();
        points.unpersist();

        return clusteredPointsAndCentroids;
    }

    /**
     * naive kmeans clustering  for the top countries hitted by covid trendline coeff for each month
     * concurrent on each month
     * @param sc        java spark context used to broadcast common variable values in maps
     * @param top50CountriesMonth   list of <monthID,List of sorted points to cluster <trendline,countryName> (used only for euristic basedi init)
     * @param countryMonthCasesTop50 list of points to cluster <trendlineValue,countryName>
     * @param firtsMonthId          first month number used in processed dataset
     * @param centroidsInit         centroids initialization method: either RAND for fully random init or DISTANTIATED_RAND for euristic based init
     * @param tolerationCentroidsDistance   tolleration distance value among old and new centroids point to stop the algoritm
     * @param firstYear
     * @return  csv of classified points: monthNumber,countryName,clustered centroids point
     */
    public String computeNaiveKMeansConcurrent(JavaSparkContext sc, List<JavaPairRDD<Integer, List<Tuple2<Double, String>>>> top50CountriesMonth,
                                               JavaPairRDD<Integer, List<Tuple2<Double, String>>> countryMonthCasesTop50, int firtsMonthId,
                                               String centroidsInit, double tolerationCentroidsDistance, int firstYear) throws InterruptedException {

        final int numMonth = top50CountriesMonth.size();
        List<Tuple2<Double, Tuple2<Double, String>>>[] clusteredPointsAllMonths = new List[numMonth];

        //separate countries trendline points in separate rdds by month id
        JavaPairRDD<Double, String>[] monthTrendlinePoints = new JavaPairRDD[numMonth];

        for (int m = 0; m < numMonth; m++) {

            JavaPairRDD<Integer, List<Tuple2<Double, String>>> integerListJavaPairRDD = top50CountriesMonth.get(m);
            //expand each top50 countries per month into separated points <trendline,country> to cluster
            monthTrendlinePoints[m] = integerListJavaPairRDD.flatMapToPair(monthPoints -> new ArrayList<>(monthPoints._2).iterator());
        }

        HashMap<Integer, Double[]> monthCentroidsInitMap = null;

        if (centroidsInit.equals("DISTANTIATED_RND")) { //init points by euristic exploiting the pre sorted points

            List<Tuple2<Integer, Tuple2<Double, String>[]>> monthCentroidsInit =
                    countryMonthCasesTop50.mapToPair(top50Month -> centroidsInitSeparatedGroups(top50Month)).collect();
            countryMonthCasesTop50.unpersist();
            //extract starting centroids for each month
            monthCentroidsInitMap = new HashMap<>();

            for (Tuple2<Integer, Tuple2<Double, String>[]> centroids : monthCentroidsInit) {

                Double[] centroidsPoints = new Double[clustersNumber];

                for (int i = 0; i < clustersNumber; i++) {

                    centroidsPoints[i] = centroids._2[i]._1;
                }

                monthCentroidsInitMap.put(centroids._1, centroidsPoints);
            }
        }

        String outCsv = "";

        //for each month, cluster points with naive kmeans on separated treads
        Thread[] threads = new Thread[numMonth];

        for (int m = 0; m < numMonth; m++) {

            int monthN = m;
            HashMap<Integer, Double[]> monthCentroids = monthCentroidsInitMap;  //euristic initiated centroids

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    JavaPairRDD<Double, String> points = monthTrendlinePoints[monthN];  //points to cluster
                    points.cache();

                    int monthID = monthN;

                    ///// starting centroids
                    Double[] centroids = null;
                    if (centroidsInit.equals("RAND")) {    //random initialization of centroids
                        centroids = getStartingCentroidsRand(points, clustersNumber);
                    } else if (centroidsInit.equals("DISTANTIATED_RND")) {
                        centroids = monthCentroids.get(monthID);
                    }
                    ///gater clustered points clusterCentroid,<trendline,coutry>  by naive kmeans on this thread
                    clusteredPointsAllMonths[monthN] = kmeansNaive(centroids, tolerationCentroidsDistance, points, sc);
                }
            });

            t.start();
            threads[m] = t;
        }

        //join concurrent treads
        for (int m = 0; m < numMonth; m++) threads[m].join();

        int month;
        int oldMonth = 0;
        int year = firstYear;

        //gather all month results
        for (int m = 0; m < numMonth; m++) {


            List<Tuple2<Double, Tuple2<Double, String>>> clusterdPoints = clusteredPointsAllMonths[m];

            month = (m + firtsMonthId - 1)%12;

            if (month < oldMonth)
                year++;

            oldMonth = month;

            for (Tuple2<Double, Tuple2<Double, String>> item : clusterdPoints) {

                outCsv+= 1 + month + "-" + year  + "," + item._2._2 + "," + item._1+"\n";
            }
        }

        return outCsv;
    }

    /**
     * naive kmeans clustering  for the top countries hitted by covid trendline coeff for each month
     * sequential on each month
     * @param sc        java spark context used to broadcast common variable values in maps
     * @param top50CountriesMonth   list of <monthID,List of sorted points to cluster <trendline,countryName> (used only for euristic basedi init)
     * @param countryMonthCasesTop50 list of points to cluster <trendlineValue,countryName>
     * @param firtsMonthId          first month number used in processed dataset
     * @param centroidsInit         centroids initialization method: either RAND for fully random init or DISTANTIATED_RAND for euristic based init
     * @param tolerationCentroidsDistance   tolleration distance value among old and new centroids point to stop the algoritm
     * @param firstYear
     * @return  csv of classified points: monthNumber,countryName,clustered centroids point
     */
    public String computeNaiveKMeans(JavaSparkContext sc, List<JavaPairRDD<Integer, List<Tuple2<Double, String>>>> top50CountriesMonth,
                                     JavaPairRDD<Integer, List<Tuple2<Double, String>>> countryMonthCasesTop50, int firtsMonthId,
                                     String centroidsInit, double tolerationCentroidsDistance, int firstYear) {

        final int numMonth = top50CountriesMonth.size();
        List<Tuple2<Double, Tuple2<Double, String>>>[] clusteredPointsAllMonths = new List[numMonth];
        //separate countries trendline points in separate rdds by month id
        List<JavaPairRDD<Double, String>> monthTrendlinePoints = new ArrayList<>(); //per month countries trendline points to cluster

        for (JavaPairRDD<Integer, List<Tuple2<Double, String>>> integerListJavaPairRDD : top50CountriesMonth) {  //for each month

            //expand each top50 countries per month into separated points <trendline,country> to cluster
            monthTrendlinePoints.add(integerListJavaPairRDD.flatMapToPair(monthPoints -> new ArrayList<>(monthPoints._2).iterator()));
        }

        List<Tuple2<Integer, Tuple2<Double, String>[]>> monthCentroidsInit;
        HashMap<Integer, Double[]> monthCentroidsInitMap = null;

        if (centroidsInit.equals("DISTANTIATED_RND")) { //init points by euristic exploiting the pre sorted points

            monthCentroidsInit = countryMonthCasesTop50.mapToPair(top50Month -> centroidsInitSeparatedGroups(top50Month)).collect();
            countryMonthCasesTop50.unpersist();
            //extract starting centroids for each month
            monthCentroidsInitMap = new HashMap<>();

            for (Tuple2<Integer, Tuple2<Double, String>[]> tuple : monthCentroidsInit) {

                Double[] centroidsPoints = new Double[clustersNumber];

                for (int i = 0; i < clustersNumber; i++) {

                    centroidsPoints[i] = tuple._2[i]._1;
                }

                monthCentroidsInitMap.put(tuple._1, centroidsPoints);
            }
        }

        // for each month, cluster points with naive kmeans
        for (int m = 0; m < numMonth; m++) {

            JavaPairRDD<Double, String> points = monthTrendlinePoints.get(m);    //points to cluster
            points.cache();

            //starting centroids
            Double[] centroids = null;
            int i = 0;

            if (centroidsInit.equals("RAND")) {    //random initialization of centroids

                centroids = getStartingCentroidsRand(points, clustersNumber);

            } else if (centroidsInit.equals("DISTANTIATED_RND")) {

                centroids = monthCentroidsInitMap.get(m);
            }

            //run kmeans algo on the given set of points starting with initial centroids points
            //gather classified points: clusterCentroid,<trendline,coutry> in per month array
            clusteredPointsAllMonths[m] = kmeansNaive(centroids, tolerationCentroidsDistance, points, sc);
        }

        String outCsv = "";

        int month;
        int oldMonth = 0;
        int year = firstYear;

        //gather all month results
        for (int m = 0; m < numMonth; m++) {

            List<Tuple2<Double, Tuple2<Double, String>>> clusterdPoints = clusteredPointsAllMonths[m];

            month = (m + firtsMonthId - 1)%12;

            if (month < oldMonth)
                year++;

            oldMonth = month;

            for (Tuple2<Double, Tuple2<Double, String>> item : clusterdPoints) {

                outCsv+= 1 + month + "-" + year + "," + item._2._2 + "," + item._1+"\n";
            }
        }

        return outCsv;
    }
}
