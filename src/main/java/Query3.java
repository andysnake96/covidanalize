import entity.Country;
import kmeans.Kmeans;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static utils.Utils.*;

public class Query3 {

    public static void main(String[] args) throws InterruptedException, FileNotFoundException {

        initProperties();

        String fileSystem = prop.getProperty("HDFS");
        String pathToDataset2Query3 = prop.getProperty("PATH_TO_DATASET2_QUERY3");
        String outputPathResultsQuery3 = prop.getProperty("OUTPUT_PATH_QUERY3_RESULTS");
        int firtsMonthId = Integer.parseInt(prop.getProperty("FIRST_MONTH_ID"));
        int topCountriesPerMonth = Integer.parseInt(prop.getProperty("TOP_COUNTRIES_PER_MONTH"));
        String clusteringMode = prop.getProperty("CLUSTERING_MODE");
        String centroidsInit = prop.getProperty("CENTROIDS_INIT");
        double tolerationCentroidsDistance = Double.parseDouble(prop.getProperty("TOLERATION_CENTROIDS_DISTANCE"));
        int clustersNumber = Integer.parseInt(prop.getProperty("CLUSTERS_NUMBER"));
        int maxIterations = Integer.parseInt(prop.getProperty("MAX_ITERATIONS"));
        String sparkMaster = prop.getProperty("SPARK_MASTER");
        int firstYear = Integer.parseInt(prop.getProperty("FIRST_YEAR"));

        SparkConf conf = new SparkConf().setMaster(sparkMaster).setAppName("Query3");
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.setLogLevel("OFF");

        JavaRDD<String> covidFile = sc.textFile(pathToDataset2Query3);

        //countries are extracted and aggregate by month
        JavaRDD<Country> countryMonthCases = covidFile.flatMap(line -> parseCSV3(line, firstYear));

        //set monthNum as tuple-key and compute trendline coeff for each month entry
        //results in <monthID, List<trendLine, countryName>> grouped by key
        JavaPairRDD<Integer, Iterable<Tuple2<Double, String>>> countryGroupedMonthCases = countryMonthCases
                .mapToPair(countryMonth -> new Tuple2<>(countryMonth.getMonth(), new Tuple2<>(countryMonth.getMonthThrendline(), countryMonth.getName())))
                .groupByKey();

        //for each month, all countries are sorted by trendline and truncated to a fixed value
        JavaPairRDD<Integer, List<Tuple2<Double, String>>> countryMonthCasesTop50 = countryGroupedMonthCases
                .mapToPair(monthGroup ->topMonthlyEntries(monthGroup, topCountriesPerMonth));

        ///KMeans clustering for the top n countries for each month
        //cache months top n countries-trendline for euristic init of centroids for Naive KMeans
        if (clusteringMode.equals("NAIVE") && centroidsInit.equals("DISTANTIATED_RND"))
            countryMonthCasesTop50.cache();

        int lastMonthID = (int) countryMonthCasesTop50.count();

        //separate in different RDDs countries trendlines points by month id for clustering
        //top50CountriesNames + trendline per month
        List<JavaPairRDD<Integer, List<Tuple2<Double, String>>>> top50CountriesMonth = new ArrayList<>();

        for (int monthID = 0; monthID < lastMonthID; monthID++) {

            int finalMonthID = monthID;
            top50CountriesMonth.add(countryMonthCasesTop50.filter(line -> line._1 == finalMonthID).cache());
        }

        Kmeans kmeansSolver = new Kmeans(clustersNumber, maxIterations, topCountriesPerMonth);
        String outCsv = null;

        //countries clusterization in n clusters with kmeans by trendline, with a naive implementation or the Mllib one
        switch (clusteringMode) {

            case "MLlib":

                outCsv = kmeansSolver.computeMLlibKMeans(top50CountriesMonth, firtsMonthId, firstYear);
                break;

            case "MLlibConcurrent":

                outCsv = kmeansSolver.computeMLlibKMeansConcurrent(top50CountriesMonth, firtsMonthId, firstYear);
                break;

            case "Naive":

                outCsv = kmeansSolver.computeNaiveKMeans(sc, top50CountriesMonth, countryMonthCasesTop50,
                        firtsMonthId, centroidsInit, tolerationCentroidsDistance, firstYear);
                break;

            case "NaiveConcurrent":

                outCsv = kmeansSolver.computeNaiveKMeansConcurrent(sc, top50CountriesMonth, countryMonthCasesTop50,
                        firtsMonthId, centroidsInit, tolerationCentroidsDistance, firstYear);
                break;
        }

        System.out.println(outCsv);

        //storing results to hdfs
        try {

            FileSystem hdfs = FileSystem.get(new URI(fileSystem), new Configuration());
            Path outputhPath = new Path(outputPathResultsQuery3);
            FSDataOutputStream outputStream = hdfs.create(outputhPath , true);
            assert outCsv != null;
            outputStream.writeBytes(outCsv);
            outputStream.close();

        } catch (IOException | URISyntaxException e) {

            System.err.println("Unable to write output to hdfs");
            e.printStackTrace();
        }

        //exporting output data from hdfs to Hbase
        if (Boolean.parseBoolean(prop.getProperty("PUT_RESULTS_HBASE"))) {

            String queryProcessGroup=prop.getProperty("PROCESS_GROUP_WRITE_QUERY3");

            try {

                System.out.println("NIFI START PROCESS GROUP TO STORE RESULT TO HBASE SUCCESS: " + nifiProcessRestStart(queryProcessGroup));

            } catch (Exception e) {

                System.err.println("Unable to start nifi process group: " + queryProcessGroup);
                e.printStackTrace();
            }
        }

        sc.stop();
    }
}
