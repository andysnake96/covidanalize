import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;
import entity.WeekInfo;
import utils.Utils;
import java.net.URI;

import static utils.Utils.*;

public class Query1 {

    public static void main(String[] args) {

        initProperties();

        String fileSystem = prop.getProperty("HDFS");
        String serialization = prop.getProperty("SERIALIZATION");
        String outputPathResultsQuery1 = prop.getProperty("OUTPUT_PATH_QUERY1_RESULTS");
        String sparkMaster = prop.getProperty("SPARK_MASTER");
        SparkConf conf = new SparkConf().setMaster(sparkMaster).setAppName("Query1");
        JavaSparkContext sc = new JavaSparkContext(conf);
        sc.setLogLevel("OFF");

        //RDD tuple: <weekID, weekCovidMainData>
        JavaPairRDD<String, WeekInfo> weekInfoJavaPairRDD;

        //fill starting rdd according to selected serialization option
        if (serialization.equals("PARQUET")) {

            SparkSession sparkSession = SparkSession.builder()
                    .appName("Query1")
                    .config(conf)
                    .getOrCreate();

            String pathToDataset1 = prop.getProperty("PATH_TO_DATASET1_PARQUET");
            JavaRDD<Row> weekInfoRow = sparkSession.read().parquet(pathToDataset1).toJavaRDD();
            weekInfoJavaPairRDD = weekInfoRow.mapToPair(row -> rowToTuple(row));

        } else {

            String pathToDataset1 = prop.getProperty("PATH_TO_DATASET1_CSV");
            JavaRDD<String> dataset1 = sc.textFile(pathToDataset1);
            weekInfoJavaPairRDD = dataset1.mapToPair(line-> parseCSV1(line));
        }

        //RDD creation to get the sum of all healed and swabs for every week
        JavaPairRDD<String, WeekInfo> averages = weekInfoJavaPairRDD.reduceByKey((x, y) -> new WeekInfo(x.getWeek(),
                ((x.getHealed() + y.getHealed())), (x.getSwabs() + y.getSwabs()))).sortByKey(true);

        //RDD creation to get a csv format
        JavaRDD<String> output = averages.map(pairAveraged -> pairAveraged._1 +
                "," + pairAveraged._2.getDoubleHealed() / 7 + "," + pairAveraged._2.getDoubleSwabs() / 7);

        for (String line: output.collect())
            System.out.println(line);

        //delete hdfs output dir
        if (Boolean.parseBoolean(prop.getProperty("DELETE_HDFS_OLD_DIR"))) {

            try {

                FileSystem hdfs = FileSystem.get(new URI(fileSystem), new Configuration());
                hdfs.delete(new org.apache.hadoop.fs.Path(outputPathResultsQuery1), true);

            } catch (Exception e) {

                System.err.println("Unable to delete old folder at" + outputPathResultsQuery1);
                e.printStackTrace();
            }
        }

        //storing results to hdfs
        output.saveAsTextFile(outputPathResultsQuery1);

        //exporting output data from hdfs to Hbase
        if (Boolean.parseBoolean(prop.getProperty("PUT_RESULTS_HBASE"))) {

            String queryProcessGroup = prop.getProperty("PROCESS_GROUP_WRITE_QUERY1");

            try {

                System.out.println("NIFI STORE RESULT TO HBASE SUCCESS: " + nifiProcessRestStart(queryProcessGroup));

            } catch (Exception e) {

                System.err.println("Unable to start nifi process group: " + queryProcessGroup);
                e.printStackTrace();
            }
        }

        sc.stop();
    }

}
