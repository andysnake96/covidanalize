package entity;

import java.io.Serializable;

public class WeekInfo implements Serializable {

    private String weekKey;
    private Integer healed;
    private Integer swabs;

    public WeekInfo(String week, Integer healed, Integer swabs) {
        this.weekKey = week;
        this.healed = healed;
        this.swabs = swabs;
    }

    public WeekInfo(Integer healed, Integer swabs) {
        this.healed = healed;
        this.swabs = swabs;
    }

    public String getWeek() {
        return weekKey;
    }

    public Integer getHealed() {
        return healed;
    }

    public Integer getSwabs() {
        return swabs;
    }

    public Double getDoubleHealed() {
        return Double.valueOf(healed);
    }

    public Double getDoubleSwabs() {
        return Double.valueOf(swabs);
    }
}
