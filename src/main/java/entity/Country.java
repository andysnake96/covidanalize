package entity;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import java.io.Serializable;
import java.util.List;

public class Country implements Serializable {

    private String name;
    private Integer month;
    private double trendline;
    private String continent;
    private List<Integer[]> aggregateCases;
    private Integer[] casesOfWeek;
    private int weekIdOfCases;
    private Integer[] increments;

    public Country(Integer[] increments, String name, Integer month) {
        this.increments = increments;
        this.name = name;
        this.month=month;
    }

    public Country(String continent, Integer[] casesOfWeek, int weekIdOfCases) {
        this.continent = continent;
        this.casesOfWeek = casesOfWeek;
        this.weekIdOfCases = weekIdOfCases;
    }

    public Country(String continent, String name, double trendline, List<Integer[]> aggregateCases) {
        this.name = name;
        this.continent = continent;
        this.trendline = trendline;
        this.aggregateCases = aggregateCases;
    }

    public Country(String name, String continent, double trendline, Integer[] cases, Integer[] increments) {
        this.name = name;
        this.continent = continent;
        this.trendline = trendline;
        this.casesOfWeek = cases;
        this.increments = increments;
    }

    public List<Integer[]> getAggregateCases() {
        return aggregateCases;
    }

    public Integer getMonth() {
        return month;
    }

    public String getName() {
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public double getTrendline() {
        return trendline;
    }

    public  Integer[] getCasesOfWeek() {
        return casesOfWeek;
    }

    public int getWeekIdOfCases() {
        return weekIdOfCases;
    }

    public Integer[] getIncrements() {
        return increments;
    }

    public void setIncrements(Integer[] increments) {
        this.increments = increments;
    }

    public Double getMonthThrendline() {

        SimpleRegression linearRegression=new SimpleRegression();
        Integer[] increments = getIncrements();

        for(int i=0; i < increments.length; i++){

            linearRegression.addData(i, increments[i]);
        }

        return linearRegression.getSlope();
    }
}