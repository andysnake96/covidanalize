# Progetto 1 SABD - Corsi, Di Iorio

Progetto per rispondere ad alcune query riguardanti la pandemia Covid-19 utilizzando Apache Spark.

## Requisiti

Hadoop-2.7.7\
Nifi-1.11.4\
HBase-2.2.4

## Config

Per eseguire il codice è necessario configurare Hadoop, Nifi ed HBase, quest'ultimo in maniera
distribuita su Hadoop. <br>Per quanto riguarda Nifi è sufficiente importare il file "flow.xml.gz" contenuto
in "scripts", nella cartella di configurazione "conf" di Nifi, andando a modificare da interfaccia grafica
i path degli script python e delle configurazioni di Hadoop ed HBase. <br>Successivamente devono essere eseguiti
i gruppi di processori per l'importazione ed il processamento dei dataset su HDFS, o attraverso l'interfaccia grafica,
o tramite i relativi script. <br>
Infine, tramite HBase Shell, è necessario creare tre tabelle per l'esportazione dei dati:

```bash
create 'output_1','data'
create 'output_2','data'
create 'output_3','data'
```

Modificare il file "app.properties" secondo le proprie esigenze e configurazioni.

## Usage

Una volta effettuata l'importazione ed il processamento dei dataset, è possibile procedere con l'esecuzione
delle query, sia tramite IDE, sia tramite script, sia nel seguente modo:

```bash
$SPARK_HOME/bin/spark-submit --class "Query1" ../target/covidanalize-1.0.jar
$SPARK_HOME/bin/spark-submit --class "Query2" ../target/covidanalize-1.0.jar
$SPARK_HOME/bin/spark-submit --class "Query3" ../target/covidanalize-1.0.jar
```
