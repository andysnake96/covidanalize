import sys,datetime
from calendar import monthrange
import traceback
from java.nio.charset import StandardCharsets
from org.apache.commons.io import IOUtils
from org.apache.nifi.processor.io import StreamCallback
from org.python.core.util import StringUtil

#TARGET TRASFORM TO
#Continent,Location(State||Country),cumulativeFirstDayOfFullMonth,.....cumulativeLastOfLastFullMonth

#used state_continent dictionary unserialized csv for quick mapping from geoname query

#TOOGLE
STATE_CONTINENT_CSV="/home/luigi/IdeaProjects/covidanalize/scripts/processing/state_continent.csv"
#STATE_CONTINENT_CSV="/home/try/DATA/prjUni/covidanalize/scripts/processing/state_continent.csv"

stateContinent=dict()
for k,w in [ x[:-1].replace(" ","").split(",") for x in open(STATE_CONTINENT_CSV).readlines() ]:    stateContinent[k]=w

class TransformCallback(StreamCallback):
    def __init__(self):
        pass
    
    def process(self, inputStream, outputStream):
        try:
            # Read input FlowFile content
            input_text = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
            csvLines=input_text.split("\n")[1:-1]   #remove header and last empty line
            #cut the dataset from the first day of first full month(february)
            FIRST_DAY_OF_DATEST=datetime.date(2020,1,22)
            OFFSET_TO_FIRST_FULLMONTH_DAY,OFFSET_TO_LAST_FULLMONTH_DAY=0,0
            if FIRST_DAY_OF_DATEST.day!=1: OFFSET_TO_FIRST_FULLMONTH_DAY=1+monthrange(2020,FIRST_DAY_OF_DATEST.month)[1]-FIRST_DAY_OF_DATEST.day
            DAYS_OF_DATASET=len(csvLines[0].split(",")[4:])
            LAST_DAY_OF_DATASET=FIRST_DAY_OF_DATEST+datetime.timedelta(days=DAYS_OF_DATASET)
            if LAST_DAY_OF_DATASET.day!=monthrange(2020,LAST_DAY_OF_DATASET.month)[1]: OFFSET_TO_LAST_FULLMONTH_DAY=LAST_DAY_OF_DATASET.day-1 #back to prev last month day
            ### Transform content
            outLines=list()
            for l in csvLines:
                #handle states with "name", replaceing quoted name with first piece, on the left of  a colon
                if '"' in l:
                    quoteIdxL,quoteIdxR=l.index('"'),l.rindex('"')
                    quotedName=l[quoteIdxL:quoteIdxR].split('"')[-1]
                    locName=quotedName.split(",")[0]
                    l=l[:quoteIdxL]+locName+l[quoteIdxR+1:] #line cleaned of quotes
                fields=l.split(",")
                state=fields[0]
                country=fields[1]
                locName=state
                if len(locName)==0: locName=country
                continent=stateContinent[locName.replace(" ","")]
                if continent=="SA" or continent=="NA": continent="A"    #merge in 1! america continent
                ### SET THE NEW FIELDS
                newFields=[continent,locName]

                cumulativeCases=fields[4:]
                ############################ clean non cumulative data
                for x in range(OFFSET_TO_FIRST_FULLMONTH_DAY,len(cumulativeCases)-OFFSET_TO_LAST_FULLMONTH_DAY):
                    increment=int(cumulativeCases[x])-int(cumulativeCases[x-1])
                    if increment<0: cumulativeCases[x]=cumulativeCases[x-1] #clean outliers values
                #cut the dataset from first full month (feb) to last full month day
                cumulativeCases=cumulativeCases[OFFSET_TO_FIRST_FULLMONTH_DAY:]
                #if OFFSET_TO_LAST_FULLMONTH_DAY!=0: cumulativeCases=cumulativeCases[:-OFFSET_TO_LAST_FULLMONTH_DAY]
                newFields.extend(cumulativeCases)
                outLines.append(",".join(newFields))
            ##
            ### Write output content
            output_text = "\n".join(outLines)
            outputStream.write(StringUtil.toBytes(output_text))
        except:
            traceback.print_exc(file=sys.stdout)
            raise

flowFile = session.get()
if flowFile != None:
    flowFile = session.write(flowFile, TransformCallback())
    # Finish by transferring the FlowFile to an output relationship
    session.transfer(flowFile, REL_SUCCESS)

