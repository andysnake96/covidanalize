import sys,datetime
import traceback
from java.nio.charset import StandardCharsets
from org.apache.commons.io import IOUtils
from org.apache.nifi.processor.io import StreamCallback
from org.python.core.util import StringUtil

#TARGET: #weekID_of_day,healedNew,swabsNew
#extract week id from dates and extract new swabs/healed. cutting the dataset to start from first sunday and end to last saturday to have complete weeks

class TransformCallback(StreamCallback):
    def __init__(self):
        pass

    def process(self, inputStream, outputStream):
        try:
            # Read input FlowFile content
            input_text = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
            outLines=list()
            csvLines=input_text.split("\n")[1:-2]
            #split fields for each line
            csvFieldsPerLine=[line.split(",") for line in csvLines]
            #make the dataset start from first sunday and end to last saturday
            START_WEEK_DAY=6
            FIRST_DAY_DATASET=datetime.datetime.strptime(csvFieldsPerLine[0][0],"%Y-%m-%dT%H:%M:%S")
            FIRST_WEEKDAY_DATASET=FIRST_DAY_DATASET.weekday()
            LAST_WEEKDAY_DATASET=(FIRST_WEEKDAY_DATASET+len(csvFieldsPerLine)-1)%7
            OFFSET_TO_FIRST_WEEKDAY,OFFSET_TO_LAST_WEEKDAY=0,0
            #start from sunday offset
            if FIRST_WEEKDAY_DATASET!=START_WEEK_DAY:  OFFSET_TO_FIRST_WEEKDAY=START_WEEK_DAY-FIRST_WEEKDAY_DATASET
            #end to last saturday
            if LAST_WEEKDAY_DATASET==START_WEEK_DAY: OFFSET_TO_LAST_WEEKDAY=1 #sunday -> saturday                      
            elif LAST_WEEKDAY_DATASET<START_WEEK_DAY-1: OFFSET_TO_LAST_WEEKDAY=LAST_WEEKDAY_DATASET+2   #back to prev saturday from curr weekday
            FIRST_TARGET_DAY_DATASET=FIRST_DAY_DATASET+datetime.timedelta(days=OFFSET_TO_FIRST_WEEKDAY)
            firstDayOfWeek=FIRST_TARGET_DAY_DATASET.strftime("%U-%y-%m-%d")
            ##outLines.append(str(OFFSET_TO_LAST_WEEKDAY)+" "+str(csvLines[-3:]))
            #take incremental values starting from the day before first START_WEEK_DAY
            healedNCumulOld,swabsNCumulOld=0,0
            if OFFSET_TO_FIRST_WEEKDAY!=0: 
                healedNCumulOld,swabsNCumulOld=int(csvFieldsPerLine[OFFSET_TO_FIRST_WEEKDAY-1][9]),int(csvFieldsPerLine[OFFSET_TO_FIRST_WEEKDAY-1][12])

            #cut dataset from first sunday to last saturady to have a compleate sequence of weeks to analize
            csvFieldsPerLine=csvFieldsPerLine[OFFSET_TO_FIRST_WEEKDAY:]
            if OFFSET_TO_LAST_WEEKDAY!=0:   csvFieldsPerLine=csvFieldsPerLine[:-OFFSET_TO_LAST_WEEKDAY]
            #extract only intresting fields: weekKey,newHealed,newSwabs
            for dayID in range(len(csvFieldsPerLine)):
                csvFields=csvFieldsPerLine[dayID]
                if dayID%7==0 and dayID>0:
                    date=datetime.datetime.strptime(csvFields[0],"%Y-%m-%dT%H:%M:%S")
                    firstDayOfWeek=date.strftime("%U-%y-%m-%d")
                weekKey=firstDayOfWeek
                #print((FIRST_weekKey_FROM_START_TARGET_DAY+dayID//7),weekKey,date)
                healedNCumul,swabsNCumul=int(csvFields[9]),int(csvFields[12])
                healedNew,swabsNew=healedNCumul-healedNCumulOld,swabsNCumul-swabsNCumulOld
                outLines.append(weekKey+","+str(healedNew)+","+str(swabsNew))
                #clean eventual outliers data, nn cumulative so current nn cumulative value is discarded
                if healedNew<0:     healedNew,healedNCumul=0,healedNCumulOld
                if swabsNew<0:      swabsNew,swabsNCumul=0,swabsNCumulOld

                #update cumulatives values for the next increments
                healedNCumulOld,swabsNCumulOld=healedNCumul,swabsNCumul 
            ### Write output content
            output_text = "\n".join(outLines)
            outputStream.write(StringUtil.toBytes(output_text))
        except:
            traceback.print_exc(file=sys.stdout)
            raise

flowFile = session.get()
if flowFile != None:
    flowFile = session.write(flowFile, TransformCallback())
    # Finish by transferring the FlowFile to an output relationship
    session.transfer(flowFile, REL_SUCCESS)

