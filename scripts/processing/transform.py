import sys
import traceback
from java.nio.charset import StandardCharsets
from org.apache.commons.io import IOUtils
from org.apache.nifi.processor.io import StreamCallback
from org.python.core.util import StringUtil

#TARGET TRASFORM TO
#Continent,Location(State||Country),1/22/20,1/23/20,1/24/20,.....

#used state_continent dictionary unserialized csv for quick mapping from geoname query

#TOOGLE
STATE_CONTINENT_CSV="/home/luigi/IdeaProjects/covidanalize/scripts/processing/state_continent.csv"
#STATE_CONTINENT_CSV="/home/try/DATA/prjUni/covidanalize/scripts/processing/state_continent.csv"

stateContinent=dict()
for k,w in [ x[:-1].replace(" ","").split(",") for x in open(STATE_CONTINENT_CSV).readlines() ]:    stateContinent[k]=w

class TransformCallback(StreamCallback):
    def __init__(self):
        pass

    def process(self, inputStream, outputStream):
        try:
            # Read input FlowFile content
            input_text = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
            csvLines=input_text.split("\n")
            ### Transform content
            outLines=list()
            for l in csvLines[1:-1]:
                #handle states with "name", replaceing quoted name with first piece, on the left of  a colon
                if '"' in l:
                    quoteIdxL,quoteIdxR=l.index('"'),l.rindex('"')
                    quotedName=l[quoteIdxL:quoteIdxR].split('"')[-1]
                    locName=quotedName.split(",")[0]
                    l=l[:quoteIdxL]+locName+l[quoteIdxR+1:] #line cleaned of quotes
                fields=l.split(",")
                state=fields[0]
                country=fields[1]
                locName=state
                if len(locName)==0: locName=country
                continent=stateContinent[locName.replace(" ","")]
                if continent=="SA" or continent=="NA": continent="A"    #merge in 1! america continent
                ### SET THE NEW FIELDS
                newFields=[continent,locName]

                cumulativeCases=fields[4:]
                ############################ clean non cumulative data
#                 wrongValue=-1   #set differently if founded a wrong value in the cumulativeCases
#                 for x in range(1,len(cumulativeCases)):
#                     increment=cumulativeCases[x]-cumulativeCases[x-1]
#                     if wrongValue!=-1:
#                     if increment<0:     #DIRTY VALUE FOUNDED
#                         wrongValue=cumulativeCases[x]
#                         cumulativeCases[x]=cumulativeCases[x-1] #reset with prev, not wrong value
                newFields.extend(cumulativeCases)
                outLines.append(",".join(newFields))
            ##
            ### Write output content
            output_text = "\n".join(outLines)
            outputStream.write(StringUtil.toBytes(output_text))
        except:
            traceback.print_exc(file=sys.stdout)
            raise

flowFile = session.get()
if flowFile != None:
    flowFile = session.write(flowFile, TransformCallback())
    # Finish by transferring the FlowFile to an output relationship
    session.transfer(flowFile, REL_SUCCESS)

